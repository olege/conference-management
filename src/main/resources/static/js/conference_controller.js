'use strict';

angular.module('myApp').controller('ConferenceController', ['$scope', '$filter', 'ConferenceService', function ($scope, $filter, ConferenceService) {
    var self = this;
    const CONST_HOUR = 60 * 60000;
    self.conference = {id: null, name: '', date: ''};
    self.participant = {id: null, name: '', birthday: null, conferenceId: null};
    self.room = {id: null, name: '', location: '', maxSeats: null};
    self.conferences = [];
    self.participants = [];
    self.selectedConference = 0;

    self.submit = submit;
    self.edit = edit;
    self.remove = remove;
    self.resetConference = resetConference;
    self.fetchParticipants = fetchParticipants;
    self.removeParticipant = removeParticipant;
    self.addParticipant = addParticipant;
    self.showRoom = showRoom;

    fetchAllConferences();

    function fetchAllConferences() {
        ConferenceService.fetchAllConferences()
            .then(
                function (d) {
                    self.conferences = d;
                },
                function (errResponse) {
                    console.error('Error while fetching Conferences');
                }
            );
    }

    function createConference(conference) {
        ConferenceService.createConference(conference)
            .then(
                fetchAllConferences,
                function (errResponse) {
                    console.error('Error while creating Conference');
                }
            );
    }

    function updateConference(conference, id) {
        ConferenceService.updateConference(conference, id)
            .then(
                fetchAllConferences,
                function (errResponse) {
                    console.error('Error while updating Conference');
                }
            );
    }

    function deleteConference(id) {
        ConferenceService.deleteConference(id)
            .then(
                fetchAllConferences,
                function (errResponse) {
                    console.error('Error while deleting Conference');
                }
            );
    }

    function submit() {
        if(validateDate()) {
            if (self.conference.id === null) {
                console.log('Saving New Conference', self.conference);
                createConference(self.conference);
            } else {
                updateConference(self.conference, self.conference.id);
                console.log('Conference updated with id ', self.conference.id);
            }
            resetConference();
        }
    }

    function edit(id) {
        console.log('id to be edited', id);
        for (var i = 0; i < self.conferences.length; i++) {
            if (self.conferences[i].id === id) {
                self.conference = angular.copy(self.conferences[i]);
                break;
            }
        }
    }

    function remove(id) {
        console.log('id to be deleted', id);
        if (self.conference.id === id) {
            resetConference();
        }
        deleteConference(id);
    }

    function resetConference() {
        self.conference = {id: null, name: '', date: ''};
        $scope.dateStr = new Date().getTime() + CONST_HOUR;
        $scope.confForm.$setPristine();
    }

    function resetParticipant() {
        self.participant = {id: null, name: '', birthday: null, conferenceId: null};
    }

    function resetRoom() {
        self.room = {id: null, name: '', location: '', maxSeats: null};
    }

    function fetchParticipants(id) {
        console.log('Fetching Participants for Conference with id ' + id);
        ConferenceService.fetchParticipants(id)
            .then(
                function (d) {
                    self.participants = d;
                    self.selectedConference = id;
                },
                function (errResponse) {
                    console.error('Error while fetching Participants for Conference with id ' + id);
                }
            );
    }

    function removeParticipant(id) {
        console.log('Removing Participant with id ' + id);
        ConferenceService.removeParticipant(id)
            .then(
                function () {
                    fetchParticipants(self.selectedConference)
                },
                function (errResponse) {
                    console.error('Error while deleting Participant with id ' + id);
                }
            );
    }

    function addParticipant() {
        self.participant.conferenceId = self.selectedConference;
        if (self.participant.birthday || Object.prototype.toString.call(self.participant.birthday) === "[object Date]") {
            var dateParts = self.participant.birthday.split('.');
            self.participant.birthday = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
        }
        console.log('Adding new Participant', self.participant);
        ConferenceService.addParticipant(self.participant)
            .then(
                function () {
                    fetchParticipants(self.selectedConference)
                },
                function (errResponse) {
                    console.error('Error while adding Participant');
                }
            );
        resetParticipant();
    }

    function showRoom(id) {
        resetRoom();
        ConferenceService.showRoom(id)
            .then(
                function (d) {
                    self.room = d;
                    $('#availableRoomModal').modal('toggle');
                },
                function (errResponse) {
                    console.error('Error while fetching Room for Conference with id ' + id);
                    $('#noRoomModal').modal('toggle');
                }
            );
    }

    $scope.dateStr = new Date().getTime() + CONST_HOUR;

    $scope.$watch('dateStr', function (date) {
        $scope.dateStr = $filter('date')(date, 'dd.MM.yyyy HH:mm');
        if ($scope.dateStr) {
            var dateParts = $scope.dateStr.split(/[ .:]+/);
            $scope.ctrl.conference.date = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0], dateParts[3], dateParts[4]);
            validateDate();
        }
    });

    function validateDate() {
        $scope.dateErrMsg = '';

        if (self.conference.date < new Date()) {
            $scope.dateErrMsg = 'Conference date and time are in the past';
            $scope.confForm.$setValidity('required', false);
            return false;
        }

        if (!self.conference.date || Object.prototype.toString.call(self.conference.date) !== "[object Date]") {
            $scope.dateErrMsg = 'Invalid date format';
            $scope.confForm.$setValidity('required', false);
            return false;
        }

        $scope.confForm.$setValidity('required', true);
        return true;
    }
}]);