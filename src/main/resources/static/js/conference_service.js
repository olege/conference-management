'use strict';

angular.module('myApp').factory('ConferenceService', ['$http', '$q', function ($http, $q) {

    var REST_SERVICE_URI = 'http://localhost:8080/conference/';

    var factory = {
        fetchAllConferences: fetchAllConferences,
        createConference: createConference,
        updateConference: updateConference,
        deleteConference: deleteConference,
        fetchParticipants: fetchParticipants,
        removeParticipant: removeParticipant,
        addParticipant: addParticipant,
        showRoom: showRoom
    };

    return factory;

    function fetchAllConferences() {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while fetching Conferences');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function createConference(conference) {
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI, conference)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while creating Conference');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }


    function updateConference(conference, id) {
        var deferred = $q.defer();
        $http.put(REST_SERVICE_URI + id, conference)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while updating Conference');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function deleteConference(id) {
        var deferred = $q.defer();
        $http.delete(REST_SERVICE_URI + id)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while deleting Conference');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function fetchParticipants(id) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI + id + '/participants')
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while fetching Participants');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function removeParticipant(id) {
        var deferred = $q.defer();
        $http.delete(REST_SERVICE_URI + 'removeparticipant/' + id)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while deleting Participant with id ' + id);
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function addParticipant(participant) {
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI + 'addparticipant', participant)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while creating Conference');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function showRoom(id) {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI + 'findavailableroom/' + id)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while finding available Room');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

}]);