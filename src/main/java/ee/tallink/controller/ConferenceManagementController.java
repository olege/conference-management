package ee.tallink.controller;

import ee.tallink.model.Conference;
import ee.tallink.model.Participant;
import ee.tallink.model.Room;
import ee.tallink.service.ConferenceManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/conference")
public class ConferenceManagementController {

    @Autowired
    ConferenceManagementService conferenceManagementService;

    @RequestMapping(value = "/")
    public ResponseEntity<List<Conference>> listAllConferences() {
        List<Conference> conferences = conferenceManagementService.findAllConferences();
        if (conferences.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(conferences, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Conference> getConference(@PathVariable("id") long id) {
        System.out.println("Fetching Conference with id " + id);
        Conference conference = conferenceManagementService.findConference(id);
        if (conference == null) {
            System.out.println("Conference with id " + id + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(conference, HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<Void> createConference(@RequestBody Conference conference, UriComponentsBuilder ucBuilder) {
        System.out.println("Creating Conference " + conference.getName());

        if (conferenceManagementService.isConferenceExist(conference)) {
            System.out.println("A Conference with name " + conference.getName() + " already exist");
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        conferenceManagementService.saveConference(conference);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/conference/{id}").buildAndExpand(conference.getId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Conference> updateConference(@PathVariable("id") long id, @RequestBody Conference conference) {
        System.out.println("Updating Conference " + id);

        Conference currentConference = conferenceManagementService.findConference(id);

        if (currentConference == null) {
            System.out.println("Conference with id " + id + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        currentConference.setName(conference.getName());
        currentConference.setDate(conference.getDate());

        conferenceManagementService.updateConference(currentConference);
        return new ResponseEntity<>(currentConference, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Conference> deleteConference(@PathVariable("id") long id) {
        System.out.println("Fetching & Deleting Conference with id " + id);

        Conference conference = conferenceManagementService.findConference(id);
        if (conference == null) {
            System.out.println("Unable to delete. Conference with id " + id + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        conferenceManagementService.deleteConference(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/", method = RequestMethod.DELETE)
    public ResponseEntity<Conference> deleteAllConferences() {
        System.out.println("Deleting All Conferences");

        conferenceManagementService.deleteAllConferences();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/{id}/participants", method = RequestMethod.GET)
    public ResponseEntity<List<Participant>> listAllParticipants(@PathVariable("id") long id) {
        System.out.println("Fetching Participants in Conference with id " + id);
        List<Participant> participants = conferenceManagementService.findParticipants(id);
        if (participants.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(participants, HttpStatus.OK);
    }

    @RequestMapping(value = "/removeparticipant/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Participant> removeParticipant(@PathVariable("id") long id) {
        System.out.println("Fetching & Deleting Participant with id " + id);

        Participant participant = conferenceManagementService.findParticipant(id);
        if (participant == null) {
            System.out.println("Unable to delete. Participant with id " + id + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        conferenceManagementService.removeParticipant(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/addparticipant", method = RequestMethod.POST)
    public ResponseEntity<Void> addParticipant(@RequestBody Participant participant) {
        System.out.println("Adding Participant " + participant.getName());

        if (conferenceManagementService.isParticipantExist(participant)) {
            System.out.println("A Participant with name " + participant.getName() + " already exist");
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        conferenceManagementService.addParticipant(participant);
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/findavailableroom/{id}", method = RequestMethod.GET)
    public ResponseEntity<Room> findAvailableRoom(@PathVariable("id") long id) {

        Room room = conferenceManagementService.findAvailableRoom(id);
        if (room == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(room, HttpStatus.OK);
    }

}
