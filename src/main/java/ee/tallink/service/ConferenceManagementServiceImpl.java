package ee.tallink.service;

import ee.tallink.dao.ConferenceDao;
import ee.tallink.dao.ParticipantDao;
import ee.tallink.dao.RoomDao;
import ee.tallink.dao.SequenceDao;
import ee.tallink.dao.sequence.SequenceIdDao;
import ee.tallink.model.Conference;
import ee.tallink.model.Participant;
import ee.tallink.model.Room;
import ee.tallink.model.Sequence;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("conferenceService")
public class ConferenceManagementServiceImpl implements ConferenceManagementService {

    private static final String CONFERENCE_SEQ_KEY = "conference";
    private static final String PARTICIPANT_SEQ_KEY = "participant";
    private static final String ROOM_SEQ_KEY = "room";

    @Autowired
    private ConferenceDao conferenceDao;

    @Autowired
    private ParticipantDao participantDao;

    @Autowired
    private RoomDao roomDao;

    @Autowired
    private SequenceIdDao sequenceIdDao;

    @Autowired
    private SequenceDao sequenceDao;

    public void initDatabase() {

        if (CollectionUtils.isEmpty(sequenceDao.findAll())) {
            sequenceDao.save(new Sequence(CONFERENCE_SEQ_KEY, 0));
            sequenceDao.save(new Sequence(PARTICIPANT_SEQ_KEY, 0));
            sequenceDao.save(new Sequence(ROOM_SEQ_KEY, 0));
        }

        if (CollectionUtils.isEmpty(roomDao.findAll())) {
            roomDao.save(new Room(sequenceIdDao.getNextSequenceId(ROOM_SEQ_KEY), "M/S Baltic Queen conference #124", "M/S Baltic Queen", 124));
            roomDao.save(new Room(sequenceIdDao.getNextSequenceId(ROOM_SEQ_KEY), "M/S Baltic Queen conference #10", "M/S Baltic Queen", 10));
            roomDao.save(new Room(sequenceIdDao.getNextSequenceId(ROOM_SEQ_KEY), "M/S Baltic Queen conference #5", "M/S Baltic Queen", 5));
        }
    }

    public List<Conference> findAllConferences() {
        return conferenceDao.findAll();
    }

    public Conference findConference(long id) {
        return conferenceDao.findOne(id);
    }

    public Conference findConference(String name) {
        return conferenceDao.findByName(name);
    }

    public Participant findParticipant(String name) {
        return participantDao.findByName(name);
    }

    public void saveConference(Conference conference) {
        conference.setId(sequenceIdDao.getNextSequenceId(CONFERENCE_SEQ_KEY));
        conferenceDao.save(conference);
    }


    public void addParticipant(Participant participant) {
        participant.setId(sequenceIdDao.getNextSequenceId(PARTICIPANT_SEQ_KEY));
        participantDao.save(participant);
    }

    public void updateConference(Conference conference) {
        conferenceDao.save(conference);
    }

    public void deleteConference(long id) {
        conferenceDao.delete(id);
    }

    public boolean isConferenceExist(Conference conference) {
        return findConference(conference.getName()) != null;
    }

    public boolean isParticipantExist(Participant participant) {
        return findParticipant(participant.getName()) != null;
    }

    public void deleteAllConferences() {
        conferenceDao.deleteAll();
    }

    public List<Participant> findParticipants(long conferenceId) {
        return participantDao.findByConferenceId(conferenceId);
    }

    public Participant findParticipant(long id) {
        return participantDao.findOne(id);
    }

    public void removeParticipant(long id) {
        participantDao.delete(id);
    }

    public Room findAvailableRoom(long conferenceId) {
        Room availableRoom = null;

        List<Room> rooms = roomDao.findAll();
        if (CollectionUtils.isNotEmpty(rooms)) {
            int participantsCount = findParticipants(conferenceId).size();

            for (Room room : rooms) {
                if (availableRoom == null) {
                    if (room.getMaxSeats() > participantsCount) {
                        availableRoom = room;
                    }
                } else {
                    if (room.getMaxSeats() >= participantsCount &&
                        room.getMaxSeats() - participantsCount < availableRoom.getMaxSeats() - participantsCount) {
                        availableRoom = room;
                    }
                }
            }
        }
        return availableRoom;
    }

}