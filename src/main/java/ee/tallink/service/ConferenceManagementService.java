package ee.tallink.service;

import ee.tallink.model.Conference;
import ee.tallink.model.Participant;
import ee.tallink.model.Room;

import java.util.List;

public interface ConferenceManagementService {

    void initDatabase();

    Conference findConference(long id);

    Conference findConference(String name);

    void saveConference(Conference conference);

    void updateConference(Conference conference);

    void deleteConference(long id);

    List<Conference> findAllConferences();

    void deleteAllConferences();

    boolean isConferenceExist(Conference conference);

    void addParticipant(Participant participant);

    boolean isParticipantExist(Participant participant);

    List<Participant> findParticipants(long conferenceId);

    Participant findParticipant(long id);

    void removeParticipant(long id);

    Room findAvailableRoom(long conferenceId);
}
