package ee.tallink.model;

import org.springframework.data.annotation.Id;

import java.util.Date;

public class Participant {

    private long id;

    private String name;

    private Date birthday;

    private long conferenceId;

    public Participant() {
    }

    public Participant(long id, String name, Date birthday, long conferenceId) {
        this.id = id;
        this.name = name;
        this.birthday = birthday;
        this.conferenceId = conferenceId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public long getConferenceId() {
        return conferenceId;
    }

    public void setConferenceId(long conferenceId) {
        this.conferenceId = conferenceId;
    }
}
