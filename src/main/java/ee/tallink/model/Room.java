package ee.tallink.model;

import org.springframework.data.annotation.Id;

public class Room {

    private long id;

    private String name;

    private String location;

    private int maxSeats;

    public Room() {
    }

    public Room(long id, String name, String location, int maxSeats) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.maxSeats = maxSeats;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getMaxSeats() {
        return maxSeats;
    }

    public void setMaxSeats(int maxSeats) {
        this.maxSeats = maxSeats;
    }
}
