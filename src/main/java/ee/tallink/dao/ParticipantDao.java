package ee.tallink.dao;

import ee.tallink.model.Participant;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ParticipantDao extends MongoRepository<Participant, Long> {

    Participant findByName(String name);

    List<Participant> findByConferenceId(long id);
}
