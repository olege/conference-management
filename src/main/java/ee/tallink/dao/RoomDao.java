package ee.tallink.dao;

import ee.tallink.model.Room;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RoomDao extends MongoRepository<Room, Long> {

}
