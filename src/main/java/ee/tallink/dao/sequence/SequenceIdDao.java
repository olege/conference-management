package ee.tallink.dao.sequence;

import ee.tallink.exception.SequenceException;

public interface SequenceIdDao {

    long getNextSequenceId(String key) throws SequenceException;
}
