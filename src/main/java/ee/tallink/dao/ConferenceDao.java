package ee.tallink.dao;

import ee.tallink.model.Conference;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ConferenceDao extends MongoRepository<Conference, Long> {

    Conference findByName(String name);
}
