package ee.tallink.dao;

import ee.tallink.model.Sequence;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SequenceDao extends MongoRepository<Sequence, Long> {

}
