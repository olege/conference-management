# CONFERENCE MANAGEMENT #

### Test project ###

This is the test Spring Boot project with Angular.js and MongoDb.

Your mongo database should have been running with standard settings.

Just run the ConferenceManagementApplication to start the application.
For the first run it would create a dummy data in the database for you.